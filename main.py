# import logging
# import sys

from flask import Flask, request, abort
from werkzeug.datastructures import FileStorage

app = Flask(__name__)

# LOG_FORMAT = "[%(levelname)s] [%(asctime)s] %(message)s"
# logging.basicConfig(stream=sys.stdout,
#                    level=logging.INFO,
#                    format=LOG_FORMAT)
# _logger = logging.getLogger()


@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello')
def hello():
    return "<p>Hello, World!</p>"

@app.get("/health")
def health():
    return "<p>health</p>"


@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return f'User {(username)}'

@app.post('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return f'Post {post_id}'

@app.post('/login')
def login():
    # _logger.info('login!')
    app.logger.info('login!')
    print(request.form.keys())

    # use get() to get the param (if not exist, return None)
    user1 = request.form.get('username')
    print(user1)
    app.logger.debug(user1)

    if 'username' not in request.form is None or 'password' not in request.form:
        # throw 400, bad request:
        app.logger.error('An error occurred')
        print('bad request!!')
        abort(400)

    # use [] to get the param (if not exist, throws Error!)
    user = request.form['username']
    print(user)
    pass2 = request.form['password']
    print(pass2)
    return 'login success'

@app.post('/upload')
def upload():
    print(request.form.keys())
    print(str(request.files))
    print(request.files.getlist('file'))
    file_: FileStorage = request.files['file']
    print(file_)
    print(file_.filename)
    return 'login success'

"""read JSON body"""
@app.post('/jsonCall')
def jsonCall():
    print(request.json)
    body = request.json
    print(body['p1'])
    return request.json