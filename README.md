# py-flask-sample


## virtual env:

https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#python_create_virtual_env

prepare environment via command line: 

    virtualenv <space name>
    Source <space name>/bin/activate   #Linuxsource <spacename>/Scripts/activate    #Windowsdeactivate
    
see explicitly the interpreter:

    virtualenv -p /usr/bin/python2.7 .venv

## Getting started

To run the application, use the `flask` command or `python -m flask`. You need to tell the Flask where your application is with the --app option.

    $ flask --app <fileName> run

or using python command (and using a specific port):
    
    python -m flask --app main run --port 8004


    $ flask --app main run
    * Serving Flask app 'hello'
    * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)

### upload file from postman

![app-upload](Screenshot 2023-02-05.jpg)
